# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)
(1...100).each do |p|
  InventoryItem.create(:name => 'Thomas', :sku => 123, :quantity => 3, :price => '4.20' )
end

(1...400).each do |p|
  InventoryItem.create(:name => 'More Tacos', :sku => 432, :quantity => 1, :price => '34.22' )
end
