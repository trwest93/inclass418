class CreateInventoryItems < ActiveRecord::Migration
  def change
    create_table :inventory_items do |t|
      t.string :name
      t.string :sku
      t.integer :quantity
      t.decimal :price

      t.timestamps null: false
    end
  end
end
